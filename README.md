# WOW Auction Scrapper
## Descripción
Intento de webapp para la consulta, almacenaje y mineria de datos sobre el auction house de World of Warcraft usando la api de battle.net que permite hacer consultas de esta índole

## Requisitos de plataforma
* Nginx (proxy reverse)
* Node.js
* mongodb (almacen de datos no-sql)

## Librerias usadas
* Express.js
