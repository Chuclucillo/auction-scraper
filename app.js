/**
 * Created by chuclucillo on 01/11/2015.
 */
var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var path = require('path');
var session = require('express-session');

server.listen(3000);

app.set('views', __dirname + '/views');
app.set('view engine', 'jade');

app.get('/', function(req, res) {
    sess = req.session;
    res.render('page', {mensaje: 'Bienvenido a al Auction Scrapper'});
});

io.on('connection', function(socket){
    console.log('a user connected');
    socket.on('chat message', function(msg){
        io.emit('chat message', msg);
        console.log('message: ' + msg);
    });
    socket.on('disconnect', function(){
        console.log('user disconnected');
    });
});

